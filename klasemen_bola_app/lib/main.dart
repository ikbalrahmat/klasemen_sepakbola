import 'package:flutter/material.dart';
import 'package:klasemen_bola_app/input_data_screen.dart';
import 'package:klasemen_bola_app/input_skor_screen.dart';
import 'package:klasemen_bola_app/klasemen_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Klasemen Sepak Bola',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        '/input_data': (context) => InputDataKlub(),
        '/input_skor': (context) => InputSkor(),
        '/klasemen': (context) => KlasemenPage(),
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Klasemen Sepak Bola'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/input_data');
              },
              child: Text('Input Data Klub'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/input_skor');
              },
              child: Text('Input Skor Pertandingan'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/klasemen');
              },
              child: Text('Tampilan Klasemen'),
            ),
          ],
        ),
      ),
    );
  }
}
