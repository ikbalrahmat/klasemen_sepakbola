import 'package:flutter/material.dart';
import 'package:klasemen_bola_app/klasemen_screen.dart';
import 'package:klasemen_bola_app/input_data_screen.dart';

class InputSkor extends StatefulWidget {
  @override
  _InputSkorState createState() => _InputSkorState();
}

class _InputSkorState extends State<InputSkor> {
  final _formKey = GlobalKey<FormState>();
  List<String> _klubList = ['Persib', 'Arema', 'Persija'];
  late String _klub1;
  late String _klub2;
  late int _score1;
  late int _score2;
  List<Map<String, dynamic>> _matchList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input Skor Pertandingan'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                children: <Widget>[
                  DropdownButton<String>(
                    value: _klub1,
                    hint: Text('Pilih Klub'),
                    onChanged: (value) {
                      setState(() {
                        _klub1 = value!;
                      });
                    },
                    items: _klubList
                        .map((klub) => DropdownMenuItem(
                              value: klub,
                              child: Text(klub),
                            ))
                        .toList(),
                  ),
                  SizedBox(width: 16),
                  Text('-'),
                  SizedBox(width: 16),
                  DropdownButton<String>(
                    value: _klub2,
                    hint: Text('Pilih Klub'),
                    onChanged: (value) {
                      setState(() {
                        _klub2 = value!;
                      });
                    },
                    items: _klubList
                        .where((klub) => klub != _klub1)
                        .map((klub) => DropdownMenuItem(
                              value: klub,
                              child: Text(klub),
                            ))
                        .toList(),
                  ),
                ],
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Score $_klub1'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Skor tidak boleh kosong';
                  }
                  if (int.tryParse(value) == null) {
                    return 'Skor harus berupa angka';
                  }
                  return null;
                },
                onSaved: (value) {
                  _score1 = int.parse(value!);
                },
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Score $_klub2'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Skor tidak boleh kosong';
                  }
                  if (int.tryParse(value) == null) {
                    return 'Skor harus berupa angka';
                  }
                  return null;
                },
                onSaved: (value) {
                  _score2 = int.parse(value!);
                },
              ),
              SizedBox(height: 16),
              ElevatedButton(
                child: Text('Save'),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();
                    Map<String, dynamic> match = {
                      'klub1': _klub1,
                      'klub2': _klub2,
                      'score1': _score1,
                      'score2': _score2,
                    };
                    if (_matchList.contains(match)) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text('Pertandingan sudah terdaftar'),
                      ));
                    } else {
                      setState(() {
                        _matchList.add(match);
                      });
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text('Pertandingan berhasil disimpan'),
                      ));
                    }
                  }
                },
              ),
              SizedBox(height: 16),
              Text('Pertandingan yang sudah disimpan:'),
              Expanded(
                child: ListView.builder(
                  itemCount: _matchList.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(
                          '${_matchList[index]['klub1']} vs ${_matchList[index]['klub2']}'),
                      subtitle: Text(
                          '${_matchList[index]['score1']} - ${_matchList[index]['score2']}'),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
