import 'package:flutter/material.dart';

class Club {
  final String name;
  final int matchesPlayed;
  final int won;
  final int drawn;
  final int lost;
  final int goalsFor;
  final int goalsAgainst;
  final int points;

  Club({
    required this.name,
    required this.matchesPlayed,
    required this.won,
    required this.drawn,
    required this.lost,
    required this.goalsFor,
    required this.goalsAgainst,
    required this.points,
  });
}

class KlasemenPage extends StatefulWidget {
  const KlasemenPage({Key? key}) : super(key: key);

  @override
  _KlasemenPageState createState() => _KlasemenPageState();
}

class _KlasemenPageState extends State<KlasemenPage> {
  List<Club> clubs = [
    Club(
      name: 'Persib',
      matchesPlayed: 2,
      won: 2,
      drawn: 0,
      lost: 0,
      goalsFor: 4,
      goalsAgainst: 0,
      points: 6,
    ),
    Club(
      name: 'Arema',
      matchesPlayed: 2,
      won: 1,
      drawn: 0,
      lost: 1,
      goalsFor: 2,
      goalsAgainst: 2,
      points: 3,
    ),
    Club(
      name: 'Persija',
      matchesPlayed: 2,
      won: 0,
      drawn: 0,
      lost: 2,
      goalsFor: 0,
      goalsAgainst: 4,
      points: 0,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Klasemen'),
      ),
      body: SingleChildScrollView(
        child: DataTable(
          columns: [
            DataColumn(label: Text('No')),
            DataColumn(label: Text('Klub')),
            DataColumn(label: Text('Ma')),
            DataColumn(label: Text('Me')),
            DataColumn(label: Text('S')),
            DataColumn(label: Text('K')),
            DataColumn(label: Text('GM')),
            DataColumn(label: Text('GK')),
            DataColumn(label: Text('Point')),
          ],
          rows: List<DataRow>.generate(
            clubs.length,
            (index) => DataRow(
              cells: [
                DataCell(Text((index + 1).toString())),
                DataCell(Text(clubs[index].name)),
                DataCell(Text(clubs[index].matchesPlayed.toString())),
                DataCell(Text(clubs[index].won.toString())),
                DataCell(Text(clubs[index].drawn.toString())),
                DataCell(Text(clubs[index].lost.toString())),
                DataCell(Text(clubs[index].goalsFor.toString())),
                DataCell(Text(clubs[index].goalsAgainst.toString())),
                DataCell(Text(clubs[index].points.toString())),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
