import 'package:flutter/material.dart';

class InputDataKlub extends StatefulWidget {
  @override
  _InputDataKlubState createState() => _InputDataKlubState();
}

class _InputDataKlubState extends State<InputDataKlub> {
  final _formKey = GlobalKey<FormState>();
  final _namaKlubController = TextEditingController();
  final _kotaKlubController = TextEditingController();
  List<String> _klubList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input Data Klub'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextFormField(
                controller: _namaKlubController,
                decoration: InputDecoration(labelText: 'Nama Klub'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Nama Klub tidak boleh kosong';
                  }
                  if (_klubList.contains(value)) {
                    return 'Klub sudah terdaftar';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _kotaKlubController,
                decoration: InputDecoration(labelText: 'Kota Klub'),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Kota Klub tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(height: 16),
              ElevatedButton(
                child: Text('Save'),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    setState(() {
                      _klubList.add(_namaKlubController.text);
                    });
                    Navigator.pop(context);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
